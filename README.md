# gitlab-redis

Deploys a Redis HA cluster consisting of `redis-server` and optioal `redis-sentinel`.

## Using

Include the recipe by adding `recipe[gitlab-redis]` to the runlist.

This installs redis binaries into `/opt/redis`, config into `/opt/redis/etc`, data into `/opt/redis/data`. Data dir can be overridden via:

```json
{
  "redis": {
    "data_dir": "/var/opt/gitlab/redis"
  }
}
```

The default configuration covers most of the basic setup for Redis. Config can be overridden or appended to via:

```json
{
  "redis": {
    "redis_conf": {
      "maxmemory": "60gb",
      "save": ["3600 1", "300 100", "60 10000"]
    }
  }
}
```

Additional configuration values can be set via secrets. For example:

```json
$ bin/gkms-vault-cat redis pre

{
  "redis-sidekiq-catchall": {
    "sentinel_conf": {
      "user": "sentinel",
      "password": "SENTINEL_REDACTED"
    },
    "redis_conf": {
      "masteruser": "replica",
      "masterauth": "REDACTED",
      "user": [
        "default off",
        "replica on ~* &* +@all >REDACTED",
        "sentinel on ~* &* +@all >SENTINEL_REDACTED",
        "console on ~* &* +@all >REDACTED",
        "webservice on ~* &* +@all >REDACTED"
      ]
    }
  }
}
```

To make connecting to `redis-cli` more convenient, a `gitlab-redis-cli` wrapper is supplied, which performs authentication for you.

Example usage:

```shell
$ sudo gitlab-redis-cli ping
PONG

$ sudo gitlab-redis-cli acl whoami
"console"

$ sudo gitlab-redis-cli --user webservice acl whoami
"webservice"
```

## Testing

The Makefile, which holds all the logic, is designed to be the same among all
cookbooks. Just set the comment at the top to include the cookbook name and
you are all set to use the below testing instructions.

### Testing locally

You can run `rspec` or `kitchen` tests directly without using provided
`Makefile`, although you can follow instructions to benefit from it.

1. Install GNU Make (`apt-get install make`). Under OS X you can achieve the
   same by `brew install make`. After this, you can see available targets of
   the Makefile just by running `make` in cookbook directory.

1. Cheat-sheet overview of current targets:

 * `make lint`: find all `*.rb` files in the current directory, excluding ones
   in `BUNDLE_PATH`, and check them with rubocop.

 * `make rspec`: the above, plus run all the rspec tests. You can use
   `bundle exec rspec -f d` to skip the lint step, but it is required on CI
   anyways, so rather please fix it early ;)

 * `make kitchen`: calculate the number of suites in `kitchen.ci.yml`, and
   run all integration tests, using the calculated number as a `concurrency`
   parameter.
