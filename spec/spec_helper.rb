require 'chefspec'
require 'chefspec/berkshelf'

def expect_secrets(keyname)
  mock_secrets_path = "test/integration/data_bags/secrets/#{keyname}.json"
  secrets = JSON.parse(File.read(mock_secrets_path))
  expect_any_instance_of(Chef::Recipe).to receive(:get_secrets)
    .with('gkms', keyname, 'key').and_return(secrets)
end
