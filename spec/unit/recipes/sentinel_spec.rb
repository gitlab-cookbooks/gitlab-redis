# Cookbook:: gitlab-redis
# Spec:: sentinel
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab-redis::sentinel' do
  context 'When all attributes are default, on Ubuntu 20.04' do
    platform 'ubuntu', '20.04'

    normal_attributes['redis']['secrets'] = {
      'backend' => 'gkms',
      'path' => 'redis',
      'key' => 'key',
    }
    normal_attributes['gce']['project']['projectId'] = 'spec_project'

    before do
      expect_secrets('redis')
      allow_any_instance_of(Chef::Node).to receive(:environment)
        .and_return('spec')
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates a symlink successfully' do
      expect(chef_run).to create_link('/opt/redis/redis-sentinel')
    end

    it 'creates a systemd service unit' do
      expect(chef_run).to create_systemd_unit('redis-sentinel.service')
      expect(chef_run).to enable_systemd_unit('redis-sentinel.service')
    end

    it 'rotates log' do
      expect(chef_run).to enable_logrotate_app('redis_sentinel').with(
        path: '/var/log/redis/redis_sentinel.log',
        options: %w(missingok compress delaycompress notifempty),
        rotate: 6,
        frequency: 'hourly'
      )
    end
  end
end
