name             'gitlab-redis'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Deploys a Redis HA cluster'
version          '0.2.0'
chef_version     '>= 14.0'
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab-redis/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab-redis'

supports 'ubuntu', '= 20.04'

# Please specify dependencies with version pin:
# depends 'cookbookname', '~> 1.0.0'
depends 'gitlab_secrets'
depends 'ark'
depends 'logrotate', '~> 2.2.0'
