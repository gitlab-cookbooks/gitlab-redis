# Cookbook:: gitlab-redis
# Recipe:: sentinel
# License:: MIT
#
# Copyright:: 2020, GitLab Inc.

base_dir = node['redis']['dir']
data_dir = node['redis']['data_dir']
log_dir = node['redis']['log_dir']
sentinel_node = node['redis']['sentinel']
cluster_name = node['redis']['cluster_name']

raw_sentinel_conf = node['redis']['sentinel']['conf'].dup
raw_sentinel_conf['sentinel monitor'] = "#{node['redis']['master_name']} #{node['redis']['master_ip']} #{node['redis']['master_port']} #{sentinel_node['quorum']}"

if sentinel_node['announce_ip_from_hostname']
  raw_sentinel_conf['sentinel resolve-hostnames'] = sentinel_node['announce_ip_from_hostname']
  raw_sentinel_conf['sentinel announce-hostnames'] = sentinel_node['announce_ip_from_hostname']
end

redis_secrets = secrets_for(node['redis']['secrets'])
if redis_secrets[cluster_name] && redis_secrets[cluster_name]['sentinel_conf']
  sentinel_acl = redis_secrets[cluster_name]['sentinel_conf']
  raw_sentinel_conf['sentinel auth-user'] = "#{node['redis']['master_name']} #{sentinel_acl['user']}"
  raw_sentinel_conf['sentinel auth-pass'] = "#{node['redis']['master_name']} #{sentinel_acl['password']}"
end

sentinel_conf = raw_sentinel_conf.flat_map do |key, values|
  values = [values] unless values.respond_to?(:map)
  values.map { |value| "#{key} #{value}" }
end.join("\n")

file "#{base_dir}/etc/sentinel.conf" do
  content sentinel_conf + "\n"
  mode '0640'
  owner node['redis']['unix_user']
  group node['redis']['unix_group']
end

redis_env = node['redis']['env'].map do |var, value|
  "\"#{var}=#{value}\""
end.join(' ')

# redis-server and redis-sentinel are the same binary
link "#{base_dir}/redis-sentinel" do
  to "#{base_dir}/redis-server"
  not_if { ::File.exist?("#{base_dir}/redis-sentinel") }
end

systemd_unit 'redis-sentinel.service' do
  content(
    {
      Unit: {
        Description: 'Run a redis-sentinel process.',
        After: 'network.target',
      },
      Service: {
        Type: 'simple',
        Environment: redis_env,
        ExecStart: "#{base_dir}/redis-sentinel #{base_dir}/etc/sentinel.conf",
        WorkingDirectory: data_dir,
        KillMode: 'process',
        Restart: 'always',
        RestartSec: '5s',
        User: node['redis']['unix_user'],
        LimitNOFILE: 'infinity',
      },
      Install: {
        WantedBy: 'multi-user.target',
      },
    }
  )
  action [:create, :enable, :start]
end

include_recipe 'logrotate::default'

logrotate_app :redis_sentinel do
  path "#{log_dir}/redis_sentinel.log"
  options %w(missingok compress delaycompress notifempty)
  rotate 6
  frequency 'hourly'
end
