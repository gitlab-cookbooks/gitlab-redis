# Cookbook:: gitlab-redis
# Recipe:: default
# License:: MIT
#
# Copyright:: 2020, GitLab Inc.

node.default['ark']['package_dependencies'] = []
include_recipe 'ark::default'

env = node.chef_environment
node.default['redis']['secrets'] = {
  backend: 'gkms',
  path: {
    path: "gitlab-#{env}-secrets/redis",
    item: "#{env}.enc",
  },
  key: {
    ring: 'gitlab-secrets',
    key: env,
    location: 'global',
  },
} if !node['cloud'].nil? && node['cloud']['provider'] == 'gce'

user node['redis']['unix_user'] do
  manage_home false
end

redis_secrets = secrets_for(node['redis']['secrets'])

cluster_name = node['redis']['cluster_name']

base_dir = node['redis']['dir']
data_dir = node['redis']['data_dir']
log_dir = node['redis']['log_dir']
redis_log_path = "#{log_dir}/redis.log"

[base_dir, log_dir].each do |dir|
  directory dir do
    owner node['redis']['unix_user']
    group node['redis']['unix_group']
    mode '0755'
    action :create
  end
end

["#{base_dir}/etc", data_dir].each do |dir|
  directory dir do
    owner node['redis']['unix_user']
    group node['redis']['unix_group']
    mode '0750'
    action :create
  end
end

raw_redis_conf = node['redis']['redis_conf'].dup
raw_redis_conf.merge!(redis_secrets[cluster_name]['redis_conf']) if redis_secrets[cluster_name] && redis_secrets[cluster_name]['redis_conf']

redis_conf = raw_redis_conf.flat_map do |key, values|
  values = [values] unless values.respond_to?(:map)
  values.map { |value| "#{key} #{value}" }
end

file "#{base_dir}/etc/redis.conf" do
  content redis_conf.join("\n") + "\n"
  mode '0640'
  owner node['redis']['unix_user']
  group node['redis']['unix_group']
end

# does not get modified by redis or sentinel during failover/restarts
# enables gitlab-redis-cli to continue retrieving password from file
file "#{base_dir}/etc/redis-password.conf" do
  content redis_conf.select { |r| r.include?('user ') }.join("\n") + "\n"
  mode '0640'
  owner node['redis']['unix_user']
  group node['redis']['unix_group']
end

ark 'redis' do
  url node['redis']['package']['url']
  version node['redis']['package']['version']
  checksum node['redis']['package']['sha256sum']
  extension 'tar.gz'
  path ::File.dirname(base_dir)
  owner node['redis']['unix_user']
  group node['redis']['unix_group']
  action :put
end

redis_env = node['redis']['env'].map do |var, value|
  "\"#{var}=#{value}\""
end.join(' ')

systemd_unit 'redis-server.service' do
  content(
    {
      Unit: {
        Description: 'Run a redis-server process.',
        After: 'network.target',
      },
      Service: {
        Type: 'simple',
        Environment: redis_env,
        ExecStart: "#{base_dir}/redis-server #{base_dir}/etc/redis.conf",
        WorkingDirectory: data_dir,
        KillMode: 'process',
        Restart: 'always',
        RestartSec: '5s',
        User: node['redis']['unix_user'],
        LimitNOFILE: 'infinity',
      },
      Install: {
        WantedBy: 'multi-user.target',
      },
    }
  )
  action [:create, :enable, :start]
end

cookbook_file "#{base_dir}/gitlab-redis-cli" do
  source 'gitlab-redis-cli'
  owner 'root'
  group 'root'
  mode '0755'
end

link '/usr/local/bin/gitlab-redis-cli' do
  to "#{base_dir}/gitlab-redis-cli"
end

include_recipe 'logrotate::default'

logrotate_app :redis do
  path redis_log_path
  options %w(missingok compress delaycompress notifempty)
  rotate 6
  frequency 'hourly'
end

include_recipe 'gitlab-redis::sentinel' if node['redis']['sentinel']['enabled']
