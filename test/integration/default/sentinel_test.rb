# Chef InSpec test for recipe gitlab-redis::sentinel

control 'basic-install-of-redis-sentinel' do
  impact 1.0
  title 'General tests for gitlab-redis cookbook'
  desc '
    This control ensures that:
      * all the basic files are created, with expected contents
      * services are enabled'

  describe systemd_service('redis-sentinel.service') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
