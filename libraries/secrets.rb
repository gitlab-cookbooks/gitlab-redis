module Gitlab
  module RedisClusterCookbook
    def secrets_for(secrets_config)
      get_secrets(secrets_config['backend'], secrets_config['path'], secrets_config['key'])
    end
  end
end

Chef::DSL::Recipe.send(:include, Gitlab::RedisClusterCookbook)
