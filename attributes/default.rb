default['redis']['dir'] = '/opt/redis'
default['redis']['data_dir'] = '/opt/redis/data'
default['redis']['log_dir'] = '/var/log/redis'
default['redis']['unix_user'] = 'gitlab-redis'
default['redis']['unix_group'] = 'gitlab-redis'

default['redis']['package'] = {}
default['redis']['package']['url'] = 'https://gitlab.com/gitlab-com/gl-infra/redis-build/-/package_files/60778301/download'
default['redis']['package']['sha256sum'] = '509d7494fd264381c41d44818a8a048fb7298be3f678162463908845ee19a5c0'
default['redis']['package']['version'] = '7.0.5'

default['redis']['redis_conf'] = {
    'logfile': '/var/log/redis/redis.log',
    'maxclients': 50000,                      # Upper bound is redis-server process max file descriptors (LimitNOFILE=infinity).
    'rename-command': 'KEYS ""',              # Prevent accidentally running the very expensive "KEYS" command.
    'repl-diskless-sync': 'yes',
    'tcp-keepalive': 60,                      # TCP keepalive interval (seconds)
    'timeout': 1200,                          # TCP idle timeout (seconds)
    'tcp-backlog': 1024,                      # Matches somaxconn default value
}

default['redis']['cluster_name'] = 'default'
default['redis']['env'] = {}
default['redis']['hostname'] = true

default['redis']['master_name'] = 'mymaster'
default['redis']['master_ip'] = '127.0.0.1'
default['redis']['master_port'] = '6379'

# Values for automated testing; in practice, you want GKMS
default['redis']['secrets']['backend'] = 'chef_vault'
default['redis']['secrets']['path'] = 'secrets'
default['redis']['secrets']['key'] = 'redis'

# Sentinel config
default['redis']['sentinel']['enabled'] = true
default['redis']['sentinel']['conf'] = {
    'logfile': '/var/log/redis/redis_sentinel.log',
    'maxclients': 40000,
    'bind': '0.0.0.0',
}

default['redis']['sentinel']['announce_ip_from_hostname'] = nil
default['redis']['sentinel']['quorum'] = 2
